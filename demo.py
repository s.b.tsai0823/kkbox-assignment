#from orgchart.core.models import base
from orgchart import OrgChart







if __name__ == "__main__":
    orgchart = OrgChart()
    orgs = orgchart.organization.list()
    print(orgs)

    app_dev = orgchart.organization.get(id=5)
    print(app_dev)
    prod_team = orgchart.organization.get(organization_code="product-dev")
    print(prod_team)
    print(app_dev.supervisor)
    print(app_dev.employees)

    thud = orgchart.employee.get(email="thud@mail")
    print(thud)
    print(thud.supervisor)

    print(thud.organizations)
    print(thud.supervisor.organizations)
