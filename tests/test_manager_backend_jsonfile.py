import pytest
import json
from orgchart.core.manager.backends.jsonfile import JsonFileWrapper
from orgchart.exceptions import *

sample_file = "/Users/derick/kkbox/assignment-orgchart/derick-orgchart/orgchart/source/employees.json"

file_data = None

with open(sample_file) as f:
    file_data = json.load(f)

def test_get():
    wrapper = JsonFileWrapper(sample_file)
    employee = wrapper.get(id=1)
    assert isinstance(employee, dict)
    assert employee['id'] == 1
    with pytest.raises(JsonFileWrapperError):
        wrapper.get()


def test_all():
    wrapper = JsonFileWrapper(sample_file)
    employees = wrapper.all()
    assert isinstance(employees,list)
    assert len(employees) == len(file_data)


def test_select():
    wrapper = JsonFileWrapper(sample_file)
    employees = wrapper.select(organization_id="president-office")
    assert isinstance(employees, list)
    assert len(employees) == 3
    with pytest.raises(JsonFileWrapperError):
        wrapper.select()
