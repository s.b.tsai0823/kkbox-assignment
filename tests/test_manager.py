from orgchart.core.manager import JsonFileManager
from orgchart.core import models
from orgchart.settings import SOURCE_FILE_DIR
import os


class Employees(models.Model):
    id = models.IntegerField(primary=True)
    employee_no = models.CharField()
    employee_name = models.CharField()
    email = models.CharField()
    onboarding_date = models.CharField()
    organization_id = models.CharField()
    job_title = models.CharField()
    gender = models.CharField()

    def __str__(self):
        return f"<{self.__class__.__name__}(id={self.id}): {self.email}>"

def test_employees_manager():
    sample_file = "employees.json"
    sample_file_path = os.path.join(SOURCE_FILE_DIR, sample_file)
    manager = JsonFileManager(Employees, sample_file_path)
    bob = manager.get(id=1)
    employee_list = manager.list()
    assert bob.id == 1
    assert len(employee_list) == 9
    assert isinstance(employee_list[0], Employees)
