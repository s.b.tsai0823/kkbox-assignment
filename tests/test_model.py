import pytest
from orgchart.core import models
from orgchart.exceptions import FieldTypeError


def test_model_field_assign_value():
    int_field = models.IntegerField()
    varchar_field = models.CharField()

    assert int_field.assign_value(3) == 3
    assert varchar_field.assign_value("some string") == "some string"
    assert varchar_field.assign_value(10205) == "10205"


def test_model_field_assign_error():
    int_field = models.IntegerField()
    with pytest.raises(FieldTypeError):
        int_field.assign_value("a")


class Product(models.Model):
    id = models.IntegerField()
    price = models.IntegerField()
    name = models.CharField(db_column="product_name")


def test_model_meta():
    product_model = Product()
    product_model_fields = product_model._meta['fields']
    assert isinstance(product_model_fields, list)
    assert 'id' in product_model_fields
    assert 'price' in product_model_fields
    assert 'name' in product_model_fields
