import json
import os
from orgchart.settings import SOURCE_FILE_DIR


def test_json_file_content():
    for filename in os.listdir(SOURCE_FILE_DIR):
        if filename.endswith('.json'):
            full_file_path = os.path.join(SOURCE_FILE_DIR, filename)
            with open(full_file_path) as f:
                data = json.load(f)
                assert isinstance(data,list)


