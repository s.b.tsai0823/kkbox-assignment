from orgchart.core.manager.base import JsonFileManager
from orgchart.models import Organization, Employees


class OrgChart():
    _instance = None
    _organizations_file = "organizations.json"
    _employees_file = "employees.json"

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self,org_file=None,employee_file=None,**kwargs):
        if org_file:
            self._organizations_file = org_file
        if employee_file:
            self._employees_file = employee_file
        self.organization = JsonFileManager(Organization, self._organizations_file)
        self.employee = JsonFileManager(Employees, self._employees_file)
