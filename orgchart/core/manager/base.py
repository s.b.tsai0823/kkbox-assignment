import abc
from orgchart.core.manager.backends import JsonFileWrapper
from orgchart.settings import SOURCE_FILE_DIR
from pathlib import Path
from orgchart.core.models.fields import Field
from orgchart.exceptions import FieldDoesNotExist


class ManagerBase(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def list(self):
        return NotImplemented

    @abc.abstractmethod
    def get(self):
        return NotImplemented

    @abc.abstractmethod
    def select(self):
        return NotImplemented

class JsonFileManager(ManagerBase):
    wrapper_class = JsonFileWrapper

    def __init__(self, model_class, json_file):
        self._model_class = model_class
        json_file_path = SOURCE_FILE_DIR / json_file
        self._file_wrapper = self.wrapper_class(json_file_path)

    def list(self):
        instance_list = []
        data_list = self._file_wrapper.all()
        return [self.assign_value_to_model(data) for data in data_list]

    def get(self, **kwargs):
        data_instance = None
        data = self._file_wrapper.get(**kwargs)
        if data:
            data_instance = self.assign_value_to_model(data)
        return data_instance

    def select(self,**kwargs):
        instance_list = []
        data_list = self._file_wrapper.select(**kwargs)
        return [self.assign_value_to_model(data) for data in data_list]

    def assign_value_to_model(self, data_dict):
        model_instance = self._model_class()
        
        for field_name in model_instance._meta['fields']:
            this_field_type = getattr(model_instance, field_name)

            if isinstance(this_field_type, Field):
                db_column = field_name
                if this_field_type.db_column:
                    db_column = this_field_type.db_column

                if db_column in data_dict:
                    column_value = data_dict[db_column]
                    field_value = this_field_type.assign_value(column_value)
                    setattr(model_instance, field_name, field_value)
                else:
                    raise FieldDoesNotExist(
                        f"{db_column} does not exist in source data.")
        model_instance._assigned = True
        return model_instance
