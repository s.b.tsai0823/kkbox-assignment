import json
from orgchart.exceptions import JsonFileWrapperError

class JsonFileWrapper():
    source_file_path = None
    source_data = None

    def __init__(self, source_file_path):
        self.source_file_path = source_file_path

    def _ensure_source_data(function):
        # 每次查詢時才載入資料, 查詢結束釋放資料
        def wrap(self, *args, **kwargs):
            if self.source_data is None:
                with open(self.source_file_path) as f:
                    self.source_data = json.load(f)
            ret = function(self, *args, **kwargs)
            self.source_data = None
            return ret
        return wrap

    @_ensure_source_data
    def get(self, **kwargs):
        if not kwargs.items():
            raise JsonFileWrapperError("You must set query.")
        ret = None
        for data_dict in self.source_data:
            ret = data_dict
            for k, v in kwargs.items():
                if data_dict[k] != v:
                    ret = None
                    break
            if ret:
                break

        return ret

    @_ensure_source_data
    def all(self):
        data_str = json.dumps(self.source_data)
        return json.loads(data_str)

    @_ensure_source_data
    def select(self, **kwargs):
        if not kwargs.items():
            raise JsonFileWrapperError("You must set query.")
        ret = []
        for data_dict in self.source_data:
            match = True
            for k, v in kwargs.items():
                if data_dict[k] != v:
                    match = False
                    break
            if match:
                ret.append(data_dict.copy())
        return ret
