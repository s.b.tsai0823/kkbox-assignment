import abc
from orgchart.exceptions import FieldTypeError

class LazyQuery:
    def __init__(self, manager_instance, query_dict, many=False):
        self.manager_instance = manager_instance
        self.query_dict = query_dict
        self.many = many

    def __call__(self):
        import importlib
        if isinstance(self.manager_instance, str):
            Orgchart = getattr(importlib.import_module(
                'orgchart.main'), 'OrgChart')
            self.manager_instance = getattr(Orgchart(), self.manager_instance)
        
        if self.many:
            return self.manager_instance.select(**self.query_dict)
        
        return self.manager_instance.get(**self.query_dict)


class Field(metaclass=abc.ABCMeta):
    """Base class for all field types"""
    def __init__(self, db_column=None, primary=False):
        self.primary = primary
        self.db_column = db_column

    @abc.abstractmethod
    def assign_value(self, value):
        return NotImplemented


class IntegerField(Field):
    @classmethod
    def assign_value(cls, value):
        if not isinstance(value, int):
            raise FieldTypeError("IntegerField must be an int.")
        
        return value


class CharField(Field):
    @classmethod
    def assign_value(cls, value):
        return str(value)
        

class ForeignKeyField(Field):
    def __init__(self, manager_class_name, lookup_column, **kwargs):
        self.manager_class_name = manager_class_name
        self.lookup_column = lookup_column
        super().__init__(**kwargs)

    def assign_value(self, value):
        query_dict = {}
        query_dict[self.lookup_column] = value
        lazy_query = LazyQuery(
            self.manager_class_name, query_dict)
        return lazy_query


class OneToManyField(Field):
    def __init__(self, manager_class_name, lookup_column, **kwargs):
        self.manager_class_name = manager_class_name
        self.lookup_column = lookup_column
        super().__init__(**kwargs)

    def assign_value(self, value):
        query_dict = {}
        query_dict[self.lookup_column] = value
        lazy_query = LazyQuery(
            self.manager_class_name, query_dict,many=True)
        return lazy_query
