from orgchart.exceptions import FieldDoesNotExist
from orgchart.core.models.fields import Field, LazyQuery


class Model():
    _assigned = False
    def __init__(self):
        # 在_meta['fields'] 紀錄自定義的Field attribute
        # manager.assign_value時依_meta['fields']觸發field.assign_value
        self._meta = {}
        self._meta['fields'] = []
        for attribute_name in dir(self):
            if attribute_name[0] != '_':
                this_field_type = getattr(self, attribute_name)
                if isinstance(this_field_type, Field):
                    self._meta['fields'].append(attribute_name)

    def __getattribute__(self, name):
        val = super().__getattribute__(name)
        if isinstance(val, LazyQuery):
            # 第一次get LazyQuery時執行，並將結果存在attribute 
            val = val()
            setattr(self, name, val)
        return val

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"<{self.__class__.__name__}>"
