from orgchart.core import models

class Employees(models.Model):
    id = models.IntegerField(primary=True)
    employee_no = models.CharField()
    employee_name = models.CharField()
    email = models.CharField()
    onboarding_date = models.CharField()
    organization = models.ForeignKeyField(
        "organization",
        lookup_column='organization_code',
        db_column='organization_id')
    job_title = models.CharField()
    gender = models.CharField()
    supervised_organizations = models.OneToManyField(
        "organization",
        lookup_column='supervisor_id',
        db_column='employee_no')

    @property
    def supervisor(self):
        if self._assigned:
            return self.organization.supervisor
        
        return None

    @property
    def organizations(self):
        if self._assigned:
            related_orgs = self.supervised_organizations
            related_orgs.insert(0, self.organization)
            return related_orgs
            
        return None
        

    def __str__(self):
        return f"<{self.__class__.__name__}(id={self.id}): {self.email}>"


class Organization(models.Model):
    id = models.IntegerField(primary=True)
    organization_name = models.CharField()
    parent = models.ForeignKeyField(
        "organization",
        lookup_column='id',
        db_column='parent_id')
    organization_code = models.CharField()
    organization_name = models.CharField()
    supervisor = models.ForeignKeyField(
        "employee",
        lookup_column='employee_no',
        db_column='supervisor_id')
    is_active = models.CharField()
    employees = models.OneToManyField(
        "employee",
        lookup_column='organization_id',
        db_column='organization_code')

    def __str__(self):
        return f"<{self.__class__.__name__}(id={self.id}): {self.organization_name}>"
