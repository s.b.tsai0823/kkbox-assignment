# Organization Chart
### Introduction
This is a project for kkbox group assignment, departments and employees data are just example.
### Quick start
```python
from orgchart import OrgChart

orgchart = OrgChart()   
orgs = orgchart.organization.list() 
# [<Organization(id=1): President Office>, 
# <Organization(id=2): Business Development Dept.>, 
# <Organization(id=3): Product Design Dept.>, ...]

## Get specific data

app_dev = orgchart.organization.get(id=5)
#<Organization(id=5): Application Development Dept.>

prod_team = orgchart.organization.get(organization_code="product-dev")
#<Organization(id=4): Product Development Div.>
```